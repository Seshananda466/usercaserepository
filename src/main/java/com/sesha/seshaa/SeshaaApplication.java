package com.sesha.seshaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeshaaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeshaaApplication.class, args);
	}

}
